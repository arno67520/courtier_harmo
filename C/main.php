<?php
	
	$F = null;

	if(isset($_GET['f'])){
	    $F = $_GET['f'];
	}
	
	switch($F){
		case "sitemap":
			include(_PATH_."V/sitemap.php");
			break;
		case "contact":
			require(_PATH_."M/agences.php");
			$data["agences"] = Agences::get_All("INNER JOIN courtier ON agence.co_id=courtier.co_id");
			include(_PATH_."V/contact.php");
			break;
		default:
			include(_PATH_."V/home.php");
			break;
	}
	
?>