<?php

/*
* INDEX ARCHI
*
* @ Arnaud Ritti
*
*/
define("_APP_FOLDER_", basename(dirname(__FILE__)), true);
define("_ROOT_", "http://".$_SERVER['SERVER_NAME']."/"._APP_FOLDER_."/", true);
define("_PATH_", $_SERVER['DOCUMENT_ROOT']."/"._APP_FOLDER_."/", true);
define("_ASSETS_", _ROOT_."assets/", true);

global $data;
global $db;
$db = new PDO('mysql:host=localhost;dbname=harmo_web;charset=utf8', 'root', '');

function datefr($date) { 
	$split = explode("-",$date); 
	$annee = $split[0]; 
	$mois = $split[1]; 
	$jour = $split[2]; 
	return "$jour"."/"."$mois"."/"."$annee"; 
} 

$C = "index";

if(isset($_GET['c'])){
    $C = $_GET['c'];
}

if($C == "index"){
    include(_PATH_."V/header.php");
    include(_PATH_."C/"."main.php");
    include(_PATH_."V/footer.php");
}elseif(file_exists(_PATH_."C/".$C.".php")){
    include(_PATH_."V/header.php");
    include(_PATH_."C/".$C.".php");
    include(_PATH_."V/footer.php");
}else{
	header('Location: '._ROOT_);
}



?>