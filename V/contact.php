<?php

foreach($data["agences"] as $enregistrement)
{
	?>
	
	
	<div class="span-4 txt-center" style="height: 250px; border: #000066 3px solid">
		<h3 class="txt-center"><?=$enregistrement->ag_nom?></h3>
		<h4 class="txt-center upper"><?=$enregistrement->ag_region?></h4>
		<p class="txt-center"><strong><?=$enregistrement->co_nom?> <?=$enregistrement->co_prenom?></strong></p>
		<address><?=$enregistrement->co_adresse?></address>
		<i class="fa fa-envelope"></i> <a href="tel:<?=$enregistrement->co_tel?>"><?=$enregistrement->co_tel?></a><br>
		<i class="fa fa-phone"></i> <a href="mailto:<?=$enregistrement->co_email?>"><?=$enregistrement->co_email?></a>
	</div>
	<?php
}

?>