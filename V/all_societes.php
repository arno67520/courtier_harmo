

<table id="societes" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Logo</th>
                <th>Nom</th>
                <th>Site internet</th>
                <th>Poid en bourse</th>
                <th>Secteur</th>
                <th>Cours de l'action</th>
                <th>Visites</th>
                <th>Couverture</th>
            </tr>
        </thead>
 
        <tbody>
            <?php
            
            foreach($data["societes"] as $enregistrement)
			{
				?>
				<tr>
	                <td><img src="<?=_ASSETS_?>img/logo/<?=$enregistrement->so_logo?>" alt="<?=$enregistrement->so_nom?>" style="width: 100px"/></td>
	                <td><?=$enregistrement->so_nom?></td>
	                <td><a href="<?=$enregistrement->so_url?>" target="_blank"><?=$enregistrement->so_url?></a></td>
	                <td><?=$enregistrement->so_poids?> %</td>
	                <td style="text-transform: capitalize"><?=$enregistrement->so_secteur?></td>
	                <td><?=$enregistrement->so_cours?> €</td>
	                <td><?=$enregistrement->so_vu?></td>
	                <td><?=$enregistrement->so_couverture?></td>
	            </tr>
				<?php
			}
            
            ?>

        </tbody>
    </table>