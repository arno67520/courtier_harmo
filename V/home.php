<div class="span-5 bd-left">
	<h3 class="txt-center">offre conseil</h3>
	<img src="<?=_ASSETS_?>img/LETTRINE_D.gif" style="float: left;margin-right: 5px"/>
	<div class="table-cell">
		<p>isponible à tout moment, votre Conseiller vous accompagne pour définir votre stratégie d'investissement en fonction de vos objectifs et de votre horizon de placement. Votre Conseiller est votre partenaire privilégié dans la mise en oeuvre de votre stratégie.</p>
		<a>En savoir plus.</a>
	</div>
	
	<h3 class="txt-center">offre gestion</h3>
	<img src="<?=_ASSETS_?>img/LETTRINE_L.gif" style="float: left;margin-right: 5px"/>
	<div class="table-cell">
		<p>es gérants de capital s'appuient sur des méthodes d'analyse et de sélection rigoureuses pour composer votre portefeuille selon vos objectifs. Ils ont accès aux analyses réalisées par BNP Paribas, et à des réseaux d'économistes et de financiers renommés.</p>
		<a>En savoir plus.</a>
	</div>
	
	<h3 class="txt-center">offre promotion</h3>
	<img src="<?=_ASSETS_?>img/LETTRINE_C.gif" style="float: left;margin-right: 5px"/>
	<div class="table-cell">
		<p>ourtier en ligne offre à tous les nouveaux abonnés un compte pour recevoir en direct et sur votre bureau les cours de la bourse. Vous pourrez ainsi mettre en oeuvre de votre stratégie en temps réel et en toute sécurité.</p>
		<a>En savoir plus.</a>
	</div>

</div>

<div class="span-7 bd-left">
	<h3 class="txt-center">Les métiers de Courtier en ligne ?</h3>

<p>Les métiers de Courtier en ligne ? C'est la gestion de portefeuille boursier sur Internet, du trading à tarif réduit, la surveillance et l'analyse des cours en continu. Courtier en ligne gère votre portefeuille.</p>

<p>Vous n'aurez plus besoin de passer vos ordres, nous les gérons à votre place, même si c'est vous qui décidez ! Nous vous donnons des informations sur les marchés et les cours, et nous vous permettons de réagir rapidement.</p>

<p>Courtier en ligne vous propose les cours de la bourse en direct, des informations, des analyses en profondeur, des pronostics, etc. De la gestion de portefeuille d'actions à la gestion de fonds privés ou de société, nous sommes spécialisés dans les actions des grandes entreprises françaises. Nous gérons aussi les SICAV & les fonds, et donnont des conseils financiers.</p>

<p>Courtier en ligne vous garantit la sécurité et la confidentialité de vos données. Nos services sont basés sur les derniers développements en terme d'accès sécurisé. Chaque jour, nos ingénieurs améliorent la sécurité des passages d'ordres de bourse.</p>

<h3 class="txt-center">Présentation de Courtier en ligne</h3>

<p>"Courtier en ligne", c'est des courtiers regroupés en agences qui travaillent ensemble. Partout en France, un courtier indépendant est à votre disposition. Cette organisation permet de mutualiser les ressources. Chaque courtier peut partager et profiter de l'expérience des autres. Chaque courtier est donc plus efficace pour vous servir. </p>

	<div class="slideshow">  
	    <ul>  
	        <li><img src="<?=_ASSETS_?>img/pub/ban0.gif" alt="" width="538" height="66" /></li>  
	        <li><img src="<?=_ASSETS_?>img/pub/ban1.gif" alt="" width="538" height="66" /></li>  
	        <li><img src="<?=_ASSETS_?>img/pub/ban2.gif" alt="" width="538" height="66" /></li>  
	        <li><img src="<?=_ASSETS_?>img/pub/ban3.gif" alt="" width="538" height="66" /></li>  
	        <li><img src="<?=_ASSETS_?>img/pub/ban4.gif" alt="" width="538" height="66" /></li>  
	        <li><img src="<?=_ASSETS_?>img/pub/ban5.gif" alt="" width="538" height="66" /></li>  
	        <li><img src="<?=_ASSETS_?>img/pub/ban6.gif" alt="" width="538" height="66" /></li>  
	        <li><img src="<?=_ASSETS_?>img/pub/ban7.gif" alt="" width="538" height="66" /></li>  
	        <li><img src="<?=_ASSETS_?>img/pub/ban8.gif" alt="" width="538" height="66" /></li>  
	        <li><img src="<?=_ASSETS_?>img/pub/ban9.gif" alt="" width="538" height="66" /></li>  
	        <li><img src="<?=_ASSETS_?>img/pub/ban10.gif" alt="" width="538" height="66" /></li>  
	        <li><img src="<?=_ASSETS_?>img/pub/ban11.gif" alt="" width="538" height="66" /></li>  
	    </ul>  
	</div> 
</div>