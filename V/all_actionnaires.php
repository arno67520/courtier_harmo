

<table id="actionnaires" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Actionnaire</th>
                <th>Date de naissance</th>
                <th>Courtier attitré</th>
            </tr>
        </thead>
 
        <tbody>
            <?php
            
            foreach($data["actionnaires"] as $enregistrement)
			{
				?>
				<tr>
	                <td><a href="<?=_ROOT_?>actionnaires/fiche/<?=$enregistrement->ac_id?>"><?=$enregistrement->ac_nom?> <?=$enregistrement->ac_prenom?></a></td>
	                <td><?=datefr($enregistrement->ac_naissance)?></td>
	                <td><a href="<?=_ROOT_?>courtiers/fiche/<?=$enregistrement->co_id?>"><?=$enregistrement->co_nom?> <?=$enregistrement->co_prenom?></a></td>
	            </tr>
				<?php
			}
            
            ?>

        </tbody>
    </table>