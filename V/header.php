<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Courtier en ligne</title>
  <meta name="viewport" content="width=950">
  <link rel="stylesheet" href="./assets/main.css">
  <link rel="stylesheet" href="//cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
    <div id="container" class="container">
        <div id="header" class="span-12">
	        <div class="txt-center span-4" >
		        <a href="<?=_ROOT_?>" ><img src="<?=_ASSETS_?>img/logo.png" id="logo" alt="Courtier en ligne"></a>
	        </div>

            <div class="span-4" style="text-align: center;font-size: 1.9em;color: #9C0800;margin-top: 1em;">
                Courtier en ligne, une solution pour tous ceux qui veulent gagner de l'argent.
            </div>
            
            <div class="span-4" style="position: relative">
                <img src="<?=_ASSETS_?>img/piece.png" alt="Courtier en ligne" style="float: right">
                <p style="position: absolute;right: 0;top: 3em;text-shadow: white 0px 0px 3px;font-weight: bold;text-align: center;font-size: 1.9em;color: #00089C;">
                    Toutes vos actions à portée de main avec Internet !
                </p>
            </div>
        </div>
        <div id="menu" class="span-12">
            <ul>
                <li><a href="<?=_ROOT_?>">Accueil</a></li>
                <li><a href="<?=_ROOT_?>actionnaires">Actionnaires</a></li>
                <li><a href="<?=_ROOT_?>societes">Sociétés du CAC40</a></li>
                <li><a href="<?=_ROOT_?>search">Recherches</a></li>
                <li><a href="<?=_ROOT_?>main_sitemap">Plan du site</a></li>
                <li><a href="<?=_ROOT_?>main_contact">Contact</a></li>
            </ul>
        </div>
        <div id="content" class="span-12">
