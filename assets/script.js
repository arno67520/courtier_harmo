 jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "date-euro-pre": function ( a ) {
        var x;
        if ( $.trim(a) !== '' ) {
            var frDatea = a;
            var frDatea2 = frDatea.split('/');
            x = (frDatea2[2] + frDatea2[1] + frDatea2[0]) * 1;
        }
        else {            x = Infinity;        }
        return x;
    },
    "date-euro-asc": function ( a, b ) {        return a - b;    },
    "date-euro-desc": function ( a, b ) {        return b - a;    }
} );

$(document).ready(function() {
    $('#societes').dataTable({
        "order": [[ 3, "desc" ]],
        "paging":   false,
        "info":   false,
        "filter":   false
    });
} );

$(document).ready(function() {
    $('#actionnaires').dataTable({
    	columnDefs: [
	       { type: 'date-euro', targets: 1 }
	     ],
        "order": [[ 0, "asc" ]],
        "paging":   false,
        "info":   false,
        "filter":   true
    });
} );

$(function(){  
  setInterval(function(){  
     $(".slideshow ul").animate({marginLeft:-538},800,function(){  
        $(this).css({marginLeft:0}).find("li:last").after($(this).find("li:first"));  
     })  
  }, 3500);  
});  


